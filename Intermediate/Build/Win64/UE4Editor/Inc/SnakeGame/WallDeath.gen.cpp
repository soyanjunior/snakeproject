// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/WallDeath.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWallDeath() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_AWallDeath_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_AWallDeath();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	void AWallDeath::StaticRegisterNativesAWallDeath()
	{
	}
	UClass* Z_Construct_UClass_AWallDeath_NoRegister()
	{
		return AWallDeath::StaticClass();
	}
	struct Z_Construct_UClass_AWallDeath_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WallMeshComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WallMeshComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AWallDeath_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWallDeath_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "WallDeath.h" },
		{ "ModuleRelativePath", "WallDeath.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWallDeath_Statics::NewProp_WallMeshComponent_MetaData[] = {
		{ "Category", "WallDeath" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "WallDeath.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWallDeath_Statics::NewProp_WallMeshComponent = { "WallMeshComponent", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWallDeath, WallMeshComponent), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWallDeath_Statics::NewProp_WallMeshComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWallDeath_Statics::NewProp_WallMeshComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AWallDeath_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWallDeath_Statics::NewProp_WallMeshComponent,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_AWallDeath_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(AWallDeath, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AWallDeath_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AWallDeath>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AWallDeath_Statics::ClassParams = {
		&AWallDeath::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AWallDeath_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AWallDeath_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AWallDeath_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AWallDeath_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AWallDeath()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AWallDeath_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AWallDeath, 2234954178);
	template<> SNAKEGAME_API UClass* StaticClass<AWallDeath>()
	{
		return AWallDeath::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AWallDeath(Z_Construct_UClass_AWallDeath, &AWallDeath::StaticClass, TEXT("/Script/SnakeGame"), TEXT("AWallDeath"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWallDeath);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
