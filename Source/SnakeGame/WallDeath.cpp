// Fill out your copyright notice in the Description page of Project Settings.
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "WallDeath.h"




// Sets default values
AWallDeath::AWallDeath()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	WallMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WallMeshComponent"));
	WallMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	WallMeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

// Called when the game starts or when spawned
void AWallDeath::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWallDeath::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWallDeath::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		Snake->Destroy();
	}
}

