// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "SnakeElementBase.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnakeBase;
class AFood;

UCLASS()
class SNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
	ASnakeBase* SnakeActor;



	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeBase> SnakeActorClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodElem;

	UPROPERTY(BlueprintReadWrite)
	AFood* FoodActor;

	UPROPERTY()
	AFood* FoodOwner;

	UFUNCTION(BlueprintCallable, Category = "SnakePawn") //������� �������� ������ �� ������
	void CreateSnakeActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
		
	UFUNCTION() //����� �������� ������ �� �����������
	void HandlePlayerHorizontalInput(float value);

	UFUNCTION() //����� �������� ������ �� ���������
	void HandlePlayerVerticalInput(float value);

	UFUNCTION() //�������� ������ ����� ����
	void HandlePlayerSpaceBarInput(float value);
	

	//���������� ��� ��������� ��������� �����
	float MinX = -1400.f;
	float MaxX = 1400.f;
	float MinY = -400.f;
	float MaxY = 400.f;
	float SpawnZ = 0.f;

	// ������� ������ ��� � ��������� �����    
	UFUNCTION()
	void AddRandomFood(int FoodNum = 1);

	float StepDelay = 1.0f; // �������� �������� ���������

	float BuferTime = 0; // ���������� �������

	int32 GameMode = 0; //���������� ������ ����

	UFUNCTION(BlueprintCallable, Category = "SnakePawn") //������� �������� ����� ���� �� ���������������
	int32 GetGameMode() const { return GameMode; }
	
	UFUNCTION(BlueprintCallable, Category = "SnakePawn") //������� �������� �����
	int32 GetScore();

	bool GamePause = false; //���������� ����� � ����

	UFUNCTION(BlueprintCallable, Category = "SnakePawn") //������� �������� ��������� ����� � blueprint
		bool GetGamePause() const 
	{ 
		return GamePause; 
	}
};
